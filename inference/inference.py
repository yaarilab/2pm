import os
import platform
import pandas as pd
from time import time
from inference.substitution import Substitution
from inference.mutability import Mutability
from inference.mmr import MMR


def main():
    # --- Platform dependant params --- #
    if platform.system() == 'Windows':
        n_proc = 4
    elif platform.system() == 'Linux':
        n_proc = 40

    data_file = os.path.join(r'run_example', r'c10mut_2.tab')
    data = pd.read_csv(data_file, nrows=10000, sep='\t')
    data = data.loc[:, ['SEQUENCE_IMGT', 'SEQUENCE_IMGT_MUTATED']]

    data.columns = ['original_sequence', 'mutated_sequence']  # Unify names

    print('Number of processes: ' + str(n_proc))

    # --- Infer substitution --- #
    t1 = time()
    substitution = Substitution(only_synonymous=True, n_proc=n_proc)
    substitution_table = substitution.profile_substitution(data)
    substitution_table = pd.read_pickle(data_file.replace('c10mut_2.tab', 'substitution.pkl'))
    t2 = time()
    print('substitution inference took: ' + '{:.2f}'.format(t2 - t1) + 's')
    print(substitution_table)

    # --- Infer substitution --- #
    t1 = time()
    mutability = Mutability(only_synonymous=True, n_proc=n_proc)
    mutability_table = mutability.profile_mutability(data, substitution_table)
    mutability_table = pd.read_pickle(data_file.replace('c10mut_2.tab', 'mutability.pkl'))
    t2 = time()
    print('mutability inference took: ' + '{:.2f}'.format(t2 - t1) + 's')
    print(mutability_table)

    # --- Infer MMR sigma --- #
    t1 = time()
    mmr = MMR(mutability_table, substitution_table, only_synonymous=True, n_proc=n_proc)
    mmr_sigma = mmr.profile_mmr(data, lower_bound=3, upper_bound=17, num_steps=100, num_iterations=1)
    t2 = time()
    print('MMR inference took: ' + '{:.2f}'.format(t2 - t1) + 's')
    print(mmr_sigma)


if __name__ == '__main__':
    main()
