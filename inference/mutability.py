import numpy as np
from pathos.multiprocessing import ProcessPool
from utils.definitions import nucleotides
from utils.model_params import empty_mutability_table
from utils.helper_functions import state_motif, is_synonymous


class Mutability:
    def __init__(self, only_synonymous=False, n_proc=1):
        self.n_proc = n_proc
        self.only_synonymous = only_synonymous
        # TODO: substitution_table should also be a property of this object

    def profile_mutability_single_sequence(self, df, mutability_table, substitution_table):
        original_sequence = np.array(list(df.original_sequence))
        mutated_sequence = np.array(list(df.mutated_sequence))

        # Padding
        pad = np.array(['N', 'N', 'N'])
        original_sequence = np.concatenate((pad, original_sequence, pad))
        mutated_sequence = np.concatenate((pad, mutated_sequence, pad))

        # TODO: test in the extreme case of single mutation per sequence!
        # TODO: document the reason and logic behind this way of inference! and justify it formally.
        #  Emphasize where it differs from S5F

        # TODO: redesign more efficient -
        #  a] vectorize instead of loops
        #  b] use pandas abilities (like value_counts)
        #  c] regex for marking motifs? for doing all at one shot

        # --- Calculate bias --- #
        bias = empty_mutability_table()

        # Count occurrence
        c_or_g_locs = np.where((original_sequence == 'C') | (original_sequence == 'G'))[0]
        for loc in c_or_g_locs:

            context = original_sequence[(loc - 2):(loc + 3)]
            motif = state_motif(context)

            if self.only_synonymous:
                orig_codon = original_sequence[((loc // 3) * 3):((loc // 3 + 1) * 3)]
                mut_codon = orig_codon.copy()
                mut_pos = loc % 3
                for n in nucleotides:
                    mut_codon[mut_pos] = n
                    if is_synonymous(orig_codon, mut_codon):
                        bias[motif] += substitution_table[motif][n]
            else:  # Any mutation accounts
                bias[motif] += 1

        # 'Normalization'
        c_sum = bias[['c_cold', 'c_neutral', 'c_hot']].sum()
        bias[['c_cold', 'c_neutral', 'c_hot']] /= c_sum

        g_sum = bias[['g_neutral', 'g_hot']].sum()
        bias[['g_neutral', 'g_hot']] /= g_sum

        # --- Aggregate mutations --- #
        valid_locs_mask = np.isin(original_sequence, nucleotides) & np.isin(mutated_sequence, nucleotides)
        mutation_locs = np.where(valid_locs_mask & (original_sequence != mutated_sequence))[0]
        mutation_locs = np.intersect1d(mutation_locs, c_or_g_locs)

        for loc in mutation_locs:
            # TODO: Please verify again with Gur how we know reading frame

            if self.only_synonymous:
                orig_codon = original_sequence[((loc // 3) * 3):((loc // 3 + 1) * 3)]
                mut_codon = orig_codon.copy()
                mut_codon[loc % 3] = mutated_sequence[loc]
                if not is_synonymous(orig_codon, mut_codon):
                    continue

            context = original_sequence[(loc - 2):(loc + 3)]
            motif = state_motif(context)
            mutability_table[motif] += 1 / bias[motif]

    def apply_profile_mutability_single_sequence(self, df, substitution_table):
        mutability_table = empty_mutability_table()
        df.apply(lambda x: self.profile_mutability_single_sequence(x, mutability_table, substitution_table), axis=1)
        return mutability_table

    def profile_mutability(self, repertoire, substitution_table):
        if self.n_proc > 1:  # Multiprocess mode
            repertoire_split = np.array_split(repertoire, self.n_proc)
            pool = ProcessPool(self.n_proc)
            mutability_table_arr = pool.map(lambda x: self.apply_profile_mutability_single_sequence(x, substitution_table),
                                            repertoire_split)
            mutability_table = sum(mutability_table_arr)
        else:  # Single process - easier to debug
            mutability_table = self.apply_profile_mutability_single_sequence(repertoire, substitution_table)

        # 'Normalization'
        c_sum = mutability_table[['c_cold', 'c_neutral', 'c_hot']].sum()
        mutability_table[['c_cold', 'c_neutral', 'c_hot']] /= c_sum

        g_sum = mutability_table[['g_neutral', 'g_hot']].sum()
        mutability_table[['g_neutral', 'g_hot']] /= g_sum

        return mutability_table


