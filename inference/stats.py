import numpy as np
from pathos.multiprocessing import ProcessPool
from utils.helper_functions import state_motif, is_synonymous
from utils.definitions import nucleotides
from utils.model_params import empty_stats_table


class Stats:
    def __init__(self, only_synonymous=False, n_proc=1):
        self.n_proc = n_proc
        self.only_synonymous = only_synonymous

    def profile_stats_single_sequence(self, df, stats_table):
        original_sequence = np.array(list(df.original_sequence))
        mutated_sequence = np.array(list(df.mutated_sequence))

        # Padding
        pad = np.array(['N', 'N', 'N'])
        original_sequence = np.concatenate((pad, original_sequence, pad))
        mutated_sequence = np.concatenate((pad, mutated_sequence, pad))

        # --- Aggregate mutations --- #
        c_or_g_locs = np.where((original_sequence == 'C') | (original_sequence == 'G'))[0]
        valid_locs_mask = np.isin(original_sequence, nucleotides) & np.isin(mutated_sequence, nucleotides)
        mutation_locs = np.where(valid_locs_mask & (original_sequence != mutated_sequence))[0]
        mutation_locs = np.intersect1d(mutation_locs, c_or_g_locs)

        for loc in mutation_locs:
            # TODO: Please verify again with Gur how we know reading frame

            context = original_sequence[(loc - 2):(loc + 3)]

            if 'N' in context or '.' in context or '-' in context:
                # TODO: What actually are 'N', '-' and '.'?
                #  learn to deal with instead of just ignoring them!
                continue

            orig_codon = original_sequence[((loc // 3) * 3):((loc // 3 + 1) * 3)]
            mut_codon = orig_codon.copy()
            mut_codon[loc % 3] = mutated_sequence[loc]
            if self.only_synonymous:
                if not is_synonymous(orig_codon, mut_codon):
                    continue
            motif = state_motif(context)
            stats_table[motif] += 1

    def apply_profile_stats_single_sequence(self, df):
        stats_table = empty_stats_table()
        df.apply(lambda x: self.profile_stats_single_sequence(x, stats_table), axis=1)
        return stats_table

    def profile_stats(self, repertoire):
        if self.n_proc > 1:  # Multiprocess mode
            repertoire_split = np.array_split(repertoire, self.n_proc)
            pool = ProcessPool(self.n_proc)
            stats_table_arr = pool.map(lambda x: self.apply_profile_stats_single_sequence(x), repertoire_split)
            stats_table = sum(stats_table_arr)
        else:  # Single process - easier to debug
            stats_table = self.apply_profile_stats_single_sequence(repertoire)

        return stats_table


