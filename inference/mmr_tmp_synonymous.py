import numpy as np
from scipy.stats import norm
from pathos.multiprocessing import ProcessPool
from utils.model_params import empty_mmr_sigma_likelihoods
from utils.helper_functions import state_motif, safe_divide, is_synonymous, calc_synonymousity_prob


class MMR:
    def __init__(self, mutability, substitution, only_synonymous=False, n_proc=1):
        self.n_proc = n_proc
        self.only_synonymous = only_synonymous
        self.mutability = mutability
        self.substitution = substitution

    def profile_mmr_single_sequence(self, df, likelihoods):
        original_sequence = np.array(list(df.original_sequence))
        mutated_sequence = np.array(list(df.mutated_sequence))

        # Padding
        pad = np.array(['N', 'N', 'N'])
        original_sequence = np.concatenate((pad, original_sequence, pad))
        mutated_sequence = np.concatenate((pad, mutated_sequence, pad))

        # Mark and weight possible MMR centers - C nucleotides
        c_centers_locs = np.where(original_sequence == 'C')[0]
        c_centers_probs = np.zeros_like(c_centers_locs, dtype=float)
        for i, loc in enumerate(c_centers_locs):

            context = original_sequence[(loc - 2):(loc + 3)]
            motif = state_motif(context)
            c_centers_probs[i] = self.mutability[motif]

        c_centers_probs /= c_centers_probs.sum()

        a_locs = np.where(original_sequence == 'A')[0]
        a_locs = a_locs[np.newaxis][np.newaxis].T
        c_centers_locs = c_centers_locs[np.newaxis].T
        a_conditioning_event_prob = (norm.cdf(a_locs + 0.5, c_centers_locs, likelihoods.index) -
                                     norm.cdf(a_locs - 0.5, c_centers_locs, likelihoods.index))
        if self.only_synonymous:
            a_locs_synonymousity_prob = calc_synonymousity_prob(original_sequence, a_locs.squeeze(), self.substitution)
            a_conditioning_event_prob = np.tensordot(a_conditioning_event_prob, a_locs_synonymousity_prob, axes=(0, 0))
        else:
            a_conditioning_event_prob = a_conditioning_event_prob.sum(axis=0)

        # Mark and weight possible MMR centers - G nucleotides
        g_centers_locs = np.where(original_sequence == 'G')[0]
        g_centers_probs = np.zeros_like(g_centers_locs, dtype=float)
        for i, loc in enumerate(g_centers_locs):

            context = original_sequence[(loc - 2):(loc + 3)]
            motif = state_motif(context)
            g_centers_probs[i] = self.mutability[motif]

        g_centers_probs /= g_centers_probs.sum()

        t_locs = np.where(original_sequence == 'T')[0]
        t_locs = t_locs[np.newaxis][np.newaxis].T
        g_centers_locs = g_centers_locs[np.newaxis].T
        t_conditioning_event_prob = (norm.cdf(t_locs + 0.5, g_centers_locs, likelihoods.index) -
                                     norm.cdf(t_locs - 0.5, g_centers_locs, likelihoods.index))
        if self.only_synonymous:
            t_locs_synonymousity_prob = calc_synonymousity_prob(original_sequence, t_locs.squeeze(), self.substitution)
            t_conditioning_event_prob = np.tensordot(t_conditioning_event_prob, t_locs_synonymousity_prob, axes=(0, 0))
        else:
            t_conditioning_event_prob = t_conditioning_event_prob.sum(axis=0)

        # Loop over target and accumulate likelihoods along grid of possible sigmas
        targets = np.where((original_sequence != mutated_sequence) &
                           ((original_sequence == 'A') | (original_sequence == 'T')))[0]
        for target in targets:

            if self.only_synonymous:
                orig_codon = original_sequence[((target // 3) * 3):((target // 3 + 1) * 3)]
                mut_codon = orig_codon.copy()
                mut_codon[target % 3] = mutated_sequence[target]
                if not is_synonymous(orig_codon, mut_codon):
                    continue

            target_base = original_sequence[target]
            if target_base in 'A':
                unconditioned_event_prob = norm.cdf(target + 0.5, c_centers_locs, likelihoods.index) - \
                                           norm.cdf(target - 0.5, c_centers_locs, likelihoods.index)
                unweighted_event_prob = safe_divide(unconditioned_event_prob, a_conditioning_event_prob)
                weighted_event_prob = c_centers_probs.dot(unweighted_event_prob)

            if target_base in 'T':
                unconditioned_event_prob = norm.cdf(target + 0.5, g_centers_locs, likelihoods.index) - \
                                           norm.cdf(target - 0.5, g_centers_locs, likelihoods.index)
                unweighted_event_prob = safe_divide(unconditioned_event_prob, t_conditioning_event_prob)
                weighted_event_prob = g_centers_probs.dot(unweighted_event_prob)

            likelihoods += np.log(weighted_event_prob)

        ## --- Aggregate mutations --- #
        #valid_locs_mask = np.isin(original_sequence, nucleotides) & np.isin(mutated_sequence, nucleotides)
        #mutation_locs = np.where(valid_locs_mask & (original_sequence != mutated_sequence))[0]
        #mutation_locs = np.intersect1d(mutation_locs, c_or_g_locs)

        #for loc in mutation_locs:
        #    # TODO: Please verify again with Gur how we know reading frame

        #    context = original_sequence[(loc - 2):(loc + 3)]

        #    if 'N' in context or '.' in context or '-' in context:
        #        # TODO: What actually are 'N', '-' and '.'?
        #        #  learn to deal with instead of just ignoring them!
        #        continue

        #    orig_codon = original_sequence[((loc // 3) * 3):((loc // 3 + 1) * 3)]
        #    mut_codon = orig_codon.copy()
        #    mut_codon[loc % 3] = mutated_sequence[loc]
        #    if self.only_synonymous:
        #        if not is_synonymous(orig_codon, mut_codon):
        #            continue
        #    motif = state_motif(context)
        #    mutability_table[motif] += 1 / bias[motif]

    def apply_profile_mmr_single_sequence(self, df, lower_bound, upper_bound, num_steps):
        likelihoods = empty_mmr_sigma_likelihoods(lower_bound, upper_bound, num_steps)
        df.apply(lambda x: self.profile_mmr_single_sequence(x, likelihoods), axis=1)
        return likelihoods

    def profile_mmr_iteration(self, repertoire, lower_bound, upper_bound, num_step):
        if self.n_proc > 1:  # Multiprocess mode
            repertoire_split = np.array_split(repertoire, self.n_proc)
            pool = ProcessPool(self.n_proc)
            likelihoods_arr = pool.map(lambda x: self.apply_profile_mmr_single_sequence(x, lower_bound, upper_bound, num_step),
                                       repertoire_split)
            likelihoods = sum(likelihoods_arr)
        else:  # Single process - easier to debug
            likelihoods = self.apply_profile_mmr_single_sequence(repertoire, lower_bound, upper_bound, num_step)

        return likelihoods

    def profile_mmr(self, repertoire, lower_bound, upper_bound, num_steps, num_iterations):
        for n in range(num_iterations):
            likelihoods = self.profile_mmr_iteration(repertoire, lower_bound, upper_bound, num_steps)
            argmax = likelihoods.argmax()
            lower_bound = likelihoods.index[max(argmax - 1, 0)]
            upper_bound = likelihoods.index[min(argmax + 1, num_steps - 1)]

        sigma = likelihoods.index[argmax]
        return sigma
