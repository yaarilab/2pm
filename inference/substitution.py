import numpy as np
from pathos.multiprocessing import ProcessPool
from utils.definitions import nucleotides
from utils.model_params import empty_substitution_table
from utils.helper_functions import state_motif, is_only_synonymous


class Substitution:
    def __init__(self, only_synonymous=False, n_proc=1):
        self.n_proc = n_proc
        self.any = not only_synonymous

    def profile_substitution_single_sequence(self, df, substitution_table):
        original_sequence = np.array(list(df.original_sequence))
        mutated_sequence = np.array(list(df.mutated_sequence))

        # Padding
        pad = np.array(['N', 'N', 'N'])
        original_sequence = np.concatenate((pad, original_sequence, pad))
        mutated_sequence = np.concatenate((pad, mutated_sequence, pad))

        # Find mutations
        valid_locs_mask = np.isin(original_sequence, nucleotides) & np.isin(mutated_sequence, nucleotides)
        mutation_locs = np.where(valid_locs_mask & (original_sequence != mutated_sequence))[0]

        for loc in mutation_locs:
            # TODO: Please verify again with Gur how we know reading frame

            orig_codon = original_sequence[((loc // 3) * 3):((loc // 3 + 1) * 3)]
            mut_codon = mutated_sequence[((loc // 3) * 3):((loc // 3 + 1) * 3)]
            all_are_valid_nucleotides = all(np.isin(orig_codon, nucleotides) & np.isin(mut_codon, nucleotides))
            if all_are_valid_nucleotides:
                if is_only_synonymous(orig_codon, mut_position=(loc % 3)) or self.any:
                    context = original_sequence[(loc - 2):(loc + 3)]
                    motif = state_motif(context)
                    mutated_base = mutated_sequence[loc]
                    substitution_table[motif][mutated_base] += 1

    def apply_profile_substitution_single_sequence(self, df):
        substitution_table = empty_substitution_table()
        df.apply(lambda x: self.profile_substitution_single_sequence(x, substitution_table), axis=1)
        return substitution_table

    def profile_substitution(self, repertoire):
        if self.n_proc > 1:  # Multiprocess mode
            repertoire_split = np.array_split(repertoire, self.n_proc)
            pool = ProcessPool(self.n_proc)
            substitution_table_arr = pool.map(self.apply_profile_substitution_single_sequence, repertoire_split)
            substitution_table = sum(substitution_table_arr)
        else:  # Single process - easier to debug
            substitution_table = self.apply_profile_substitution_single_sequence(repertoire)
        substitution_table = substitution_table / substitution_table.sum(axis=0)  # Normalize
        return substitution_table
