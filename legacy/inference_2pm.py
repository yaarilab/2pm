import pandas as pd
import numpy as np
from scipy.stats import norm
from python.legacy.utils import mark_spots, filter_synonymous
from python.legacy.normalize_synonymous import synonymous_incidence


def infer_model_params(data, mmr_sigma_grid_coarse, mmr_sigma_grid_fine, only_synonymous=False):
    '''
    infer model params over repertoire
    :param data: pandas df with columns: 'original_sequence', 'mutated_sequence'
    :return:
    '''
    # TODO: add progress bar
    # --- Preprocess --- #
    if only_synonymous:
        data = data.apply(filter_synonymous, axis=1)

    # --- infer C/G targeting and all possible replacements probs --- #
    per_sequence = data.apply(lambda row: infer_per_sequence(row.original_sequence, row.mutated_sequence), axis=1)
    per_sequence_sum = per_sequence.sum(axis=0)
    spots_targeting_probs = per_sequence_sum[['h', 'n', 'c']]
    if only_synonymous:
        spots_targeting_probs = spots_targeting_probs / synonymous_incidence
    spots_targeting_probs = spots_targeting_probs / spots_targeting_probs.sum()

    # BER_prob = per_sequence_sum['BER_events'] / (per_sequence_sum['BER_events'] + per_sequence_sum['MMR_events'])
    # C_replacement_probs = per_sequence_sum[['CtoG', 'CtoA', 'CtoT']] / sum(per_sequence_sum[['CtoG', 'CtoA', 'CtoT']])
    # G_replacement_probs = per_sequence_sum[['GtoC', 'GtoA', 'GtoT']] / sum(per_sequence_sum[['GtoC', 'GtoA', 'GtoT']])
    # A_replacement_probs = per_sequence_sum[['AtoC', 'AtoG', 'AtoT']] / sum(per_sequence_sum[['AtoC', 'AtoG', 'AtoT']])
    # T_replacement_probs = per_sequence_sum[['TtoC', 'TtoG', 'TtoA']] / sum(per_sequence_sum[['TtoC', 'TtoG', 'TtoA']])

    # --- infer MMR sigma --- #
    spots_probs = dict(spots_targeting_probs)
    spots_probs['z'] = 0.0

    # # Coarse search
    # likelihoods = data.apply(lambda row: infer_mmr_sigma_per_sequence(row.original_sequence,
    #                                                                   row.mutated_sequence,
    #                                                                   spots_probs,
    #                                                                   mmr_sigma_grid_coarse), axis=1)
    # likelihoods = np.stack(likelihoods.values)
    # MMR_sigma = mmr_sigma_grid_coarse[np.argmax(likelihoods.sum(axis=0))]
    #
    # # Fine search
    # mmr_sigma_grid_fine = mmr_sigma_grid_fine + MMR_sigma
    # likelihoods = data.apply(lambda row: infer_mmr_sigma_per_sequence(row.original_sequence,
    #                                                                   row.mutated_sequence,
    #                                                                   spots_probs,
    #                                                                   mmr_sigma_grid_fine), axis=1)
    # likelihoods = np.stack(likelihoods.values)
    # MMR_sigma = mmr_sigma_grid_fine[np.argmax(likelihoods.sum(axis=0))]

    # --- prints --- #
    # print('--- spots targeting probs ---')
    # print(spots_probs)
    # print('--- BER prob ---')
    # print(BER_prob)
    # print('--- C replacement probs ---')
    # print(C_replacement_probs)
    # print('--- G replacement probs ---')
    # print(G_replacement_probs)
    # print('--- A replacement probs ---')
    # print(A_replacement_probs)
    # print('--- T replacement probs ---')
    # print(T_replacement_probs)
    # print('--- MMR sigma ---')
    # print(MMR_sigma)

    return {'spots_probs': spots_probs,
            # 'BER_prob': BER_prob,
            # 'BER_transition_from_C': {'G': C_replacement_probs['CtoG'],
            #                           'A': C_replacement_probs['CtoA'],
            #                           'T': C_replacement_probs['CtoT']},
            # 'BER_transition_from_G': {'C': G_replacement_probs['GtoC'],
            #                           'A': G_replacement_probs['GtoA'],
            #                           'T': G_replacement_probs['GtoT']},
            # 'MMR_transition_from_A': {'C': A_replacement_probs['AtoC'],
            #                           'G': A_replacement_probs['AtoG'],
            #                           'T': A_replacement_probs['AtoT']},
            # 'MMR_transition_from_T': {'C': T_replacement_probs['TtoC'],
            #                           'G': T_replacement_probs['TtoG'],
            #                           'A': T_replacement_probs['TtoA']},
            # 'MMR_sigma': MMR_sigma
            }


def infer_per_sequence(original_sequence, mutated_sequence):
    # --- organize in DataFrame and filter out mutations --- #
    df_all_bases = pd.DataFrame({'original_base': list(original_sequence),
                              'mutated_base': list(mutated_sequence),
                              'spot': mark_spots(original_sequence)
                              })
    df_mutated_bases = df_all_bases[df_all_bases.original_base != df_all_bases.mutated_base]

    # --- infer AID targeting probs --- #
    n_muts = df_mutated_bases.shape[0]
    empty_counts = pd.Series({'h': 0, 'n': 0, 'c': 0})  # handle zero-counts
    all_spots_counts = empty_counts.add(df_all_bases.spot.value_counts(), fill_value=0)
    mutated_spots_counts = empty_counts.add(df_mutated_bases.spot.value_counts(), fill_value=0)
    mutations_by_spots_weighted = n_muts * mutated_spots_counts / all_spots_counts

    # --- infer probs of Replacement by BER/Replication of U-G mismatch --- #
    empty_C_replacement_counts = pd.Series({'G': 0, 'A': 0, 'T': 0})  # handle zero-counts
    empty_G_replacement_counts = pd.Series({'C': 0, 'A': 0, 'T': 0})  # handle zero-counts
    C_replacement_counts = df_mutated_bases[df_mutated_bases.original_base == 'C'].mutated_base.value_counts()
    G_replacement_counts = df_mutated_bases[df_mutated_bases.original_base == 'G'].mutated_base.value_counts()
    C_replacement_counts = C_replacement_counts.add(empty_C_replacement_counts, fill_value=0)
    G_replacement_counts = G_replacement_counts.add(empty_G_replacement_counts, fill_value=0)

    # --- infer probs of Replacement by MMR --- #
    empty_A_replacement_counts = pd.Series({'C': 0, 'G': 0, 'T': 0})  # handle zero-counts
    empty_T_replacement_counts = pd.Series({'C': 0, 'G': 0, 'A': 0})  # handle zero-counts
    A_replacement_counts = df_mutated_bases[df_mutated_bases.original_base == 'A'].mutated_base.value_counts()
    T_replacement_counts = df_mutated_bases[df_mutated_bases.original_base == 'T'].mutated_base.value_counts()
    A_replacement_counts = A_replacement_counts.add(empty_A_replacement_counts, fill_value=0)
    T_replacement_counts = T_replacement_counts.add(empty_T_replacement_counts, fill_value=0)

    return pd.Series({'h': mutations_by_spots_weighted['h'],
                      'n': mutations_by_spots_weighted['n'],
                      'c': mutations_by_spots_weighted['c'],
                      'BER_events': C_replacement_counts.sum() + G_replacement_counts.sum(),
                      'MMR_events': A_replacement_counts.sum() + T_replacement_counts.sum(),
                      'CtoG': C_replacement_counts['G'],
                      'CtoA': C_replacement_counts['A'],
                      'CtoT': C_replacement_counts['T'],
                      'GtoC': G_replacement_counts['C'],
                      'GtoA': G_replacement_counts['A'],
                      'GtoT': G_replacement_counts['T'],
                      'AtoC': A_replacement_counts['C'],
                      'AtoG': A_replacement_counts['G'],
                      'AtoT': A_replacement_counts['T'],
                      'TtoC': T_replacement_counts['C'],
                      'TtoG': T_replacement_counts['G'],
                      'TtoA': T_replacement_counts['A']
                      })

def infer_mmr_sigma_per_sequence(original_sequence, mutated_sequence, spots_probs, grid):
    centers_base_by_target_base = {'T': 'G', 'A': 'C'}
    likelihoods = np.zeros_like(grid)
    # organize in DataFrame and filter out mutations
    df = pd.DataFrame({'original': list(original_sequence),
                       'mutated': list(mutated_sequence)})

    # Probability of mutation in centers (C\G)
    spots = mark_spots(original_sequence)
    probs = np.array([spots_probs[l] for l in spots])
    probs = probs / probs.sum()
    G_centers = np.where(df.original == 'G')[0][np.newaxis].T
    C_centers = np.where(df.original == 'C')[0][np.newaxis].T
    G_centers_probs = probs[G_centers.squeeze()]
    C_centers_probs = probs[C_centers.squeeze()]

    # Conditioning background , by C/G centers and A/T nucleotides
    T_locs = np.where(df.original == 'T')[0][np.newaxis][np.newaxis].T
    A_locs = np.where(df.original == 'A')[0][np.newaxis][np.newaxis].T
    T_conditioning_event_prob = (norm.cdf(T_locs + 0.5, G_centers, grid) -
                                 norm.cdf(T_locs - 0.5, G_centers, grid)).sum(axis=0)
    A_conditioning_event_prob = (norm.cdf(A_locs + 0.5, C_centers, grid) -
                                 norm.cdf(A_locs - 0.5, C_centers, grid)).sum(axis=0)

    # Loop over target and accumulate likelihoods along grid of possible sigmas
    targets = np.where((df.original != df.mutated) & (df.original.isin(['A', 'T'])))[0]
    for target in targets:
        target_base = df.original[target]
        if target_base in 'T':
            unconditioned_event_prob = norm.cdf(target + 0.5, G_centers, grid) - norm.cdf(target - 0.5, G_centers, grid)
            unweighted_event_prob = safe_divide(unconditioned_event_prob, T_conditioning_event_prob)
            weighted_event_prob = G_centers_probs.dot(unweighted_event_prob)
        if target_base in 'A':
            unconditioned_event_prob = norm.cdf(target + 0.5, C_centers, grid) - norm.cdf(target - 0.5, C_centers, grid)
            unweighted_event_prob = safe_divide(unconditioned_event_prob, A_conditioning_event_prob)
            weighted_event_prob = C_centers_probs.dot(unweighted_event_prob)

        likelihoods = likelihoods + np.log(weighted_event_prob)
    return likelihoods


def safe_divide(numerator, denominator):
    if ((denominator == 0) & (numerator != 0)).any():
        raise ZeroDivisionError
    denominator[denominator == 0] = 1.
    return numerator / denominator
