import re

# ------------------------- Definitions --------------------------- #

nucleotides = ['C', 'G', 'A', 'T']

codon_table = {'TTT': 'Phe', 'TTC': 'Phe', 'TTA': 'Leu', 'TTG': 'Leu',
               'TCT': 'Ser', 'TCC': 'Ser', 'TCA': 'Ser', 'TCG': 'Ser',
               'TAT': 'Tyr', 'TAC': 'Tyr', 'TAA': 'STOP', 'TAG': 'STOP',
               'TGT': 'Cyc', 'TGC': 'Cyc', 'TGA': 'STOP', 'TGG': 'Trp',
               'CTT': 'Leu', 'CTC': 'Leu', 'CTA': 'Leu', 'CTG': 'Leu',
               'CCT': 'Pro', 'CCC': 'Pro', 'CCA': 'Pro', 'CCG': 'Pro',
               'CAT': 'His', 'CAC': 'His', 'CAA': 'Gln', 'CAG': 'Gln',
               'CGT': 'Arg', 'CGC': 'Arg', 'CGA': 'Arg', 'CGG': 'Arg',
               'ATT': 'Ile', 'ATC': 'Ile', 'ATA': 'Ile', 'ATG': 'Met',
               'ACT': 'Thr', 'ACC': 'Thr', 'ACA': 'Thr', 'ACG': 'Thr',
               'AAT': 'Asn', 'AAC': 'Asn', 'AAA': 'Lis', 'AAG': 'Lis',
               'AGT': 'Ser', 'AGC': 'Ser', 'AGA': 'Arg', 'AGG': 'Arg',
               'GTT': 'Val', 'GTC': 'Val', 'GTA': 'Val', 'GTG': 'Val',
               'GCT': 'Ala', 'GCC': 'Ala', 'GCA': 'Ala', 'GCG': 'Ala',
               'GAT': 'Asp', 'GAC': 'Asp', 'GAA': 'Glu', 'GAG': 'Glu',
               'GGT': 'Gly', 'GGC': 'Gly', 'GGA': 'Gly', 'GGG': 'Gly'}

#           -------- spots --------
# IUPAC nucleotide code: D={A|G|T}, Y={C|T}, W={A|T}, R={A|G}, H={T|C|A}, S={G|C}
# hotspot(Odegard&Schatz): DGYW/DRCH
# coldspot(Schramm&Douek): SYC [what about the opposite?]

DGYW = '(?<=[AGT])G(?=[CT][AT])'
DRCH = '(?<=[AGT][AG])C(?=[ACT])'
SYC = '(?<=[GC][CT])C'

def mark_spots(sequence, exclude='#'):
    exclude = 'G'  # TMP DEV!!
    G_hots = re.sub(DGYW, 'h', sequence)
    C_hots = re.sub(DRCH, 'h', sequence)
    C_colds = re.sub(SYC, 'c', sequence)
    not_C_or_G = re.sub('[^CG]', 'z', sequence)
    exclude = re.sub(exclude, 'z', sequence)  # 'C' or 'G' or nothing
    marked_sequence = [max(a, b, c, d, e) for (a, b, c, d, e) in zip(G_hots, C_hots, C_colds, not_C_or_G, exclude)]
    marked_sequence = [x if x in ('h', 'c', 'z') else 'n' for x in marked_sequence]  # All the rest C|G are neutral
    return marked_sequence



def filter_synonymous(x):
    """
    Read sequence codons, and mask Non synonymous mutations.
    """
    # TODO: optimize this -
    # a. merge conditions
    # b. skip not mutated codons...
    # c. instead of looping over the whole sequence, just go to mutations

    original_sequence = x.original_sequence
    mutated_sequence = x.mutated_sequence

    for i in range(int(len(original_sequence) / 3)):
        original_codon = original_sequence[i * 3: (i + 1) * 3]
        mutated_codon = mutated_sequence[i * 3: (i + 1) * 3]

        # If non-synonymous (or if unknown because of 'N'), hide mutations
        if 'N' in original_codon:
            mutated_sequence = mutated_sequence[:(i * 3)] + original_codon + mutated_sequence[((i + 1) * 3):]
        elif codon_table[original_codon] != codon_table[mutated_codon]:
            mutated_sequence = mutated_sequence[:(i * 3)] + original_codon + mutated_sequence[((i + 1) * 3):]

    x.mutated_sequence = mutated_sequence
    return x


def print_params(params):
    for key in params.keys():
        print(key + ' | ', end='')
        if key in ['BER_prob', 'MMR_sigma']:
            print('{0:.2f}'.format(params[key]))
            continue
        for sub_key in params[key].keys():
            print(sub_key + ': {0:.6f}, '.format(params[key][sub_key]), end='')
        print('\r')


def is_synonymous(orig_codon, mut_codon):
    return codon_table[orig_codon] == codon_table[mut_codon]

