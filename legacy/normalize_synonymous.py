import pandas as pd
import itertools
import exrex
from python.legacy.utils import *

G_hots = list(exrex.generate(DGYW))
C_hots = list(exrex.generate(DRCH))
C_colds = list(exrex.generate(SYC))

fivemers = itertools.product(nucleotides, nucleotides, ['C', 'G'], nucleotides, nucleotides)
fivemers = [''.join(x) for x in fivemers]

counts = pd.DataFrame(index=['hot', 'neutral', 'cold'], columns=['syn', 'nonsyn'], data=0)

for fivemer in fivemers:
    # classify spot
    if fivemer[1:] in G_hots or fivemer[:4] in C_hots:
        spot = 'hot'
    elif fivemer[:3] in C_colds:
        spot = 'cold'
    else:
        spot = 'neutral'

    # mutate
    for m in (set(nucleotides) - set(fivemer[2])):
        mutated_fivemer = fivemer[:2] + m + fivemer[3:]

        # loop over reading frames
        for idx in range(3):
            original_codon = fivemer[idx:idx+3]
            mutated_codon = mutated_fivemer[idx:idx+3]

            # check synonymousity
            is_syn = 'syn' if codon_table[original_codon] == codon_table[mutated_codon] else 'nonsyn'

            # fill table
            counts[is_syn][spot] += 1

# calc ratio
synonymous_incidence = (counts['syn'] / counts.sum(axis=1)).to_numpy()



