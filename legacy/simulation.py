import numpy as np
from scipy.stats import norm
from python.legacy.utils import mark_spots

class Model2P:
    def __init__(self, model_params):
        for key in model_params:
            setattr(self, key, model_params[key])
        self.spots_probs['z'] = 0

    def mutate_sequence(self, sequence, n_mutations):
        # --- First phase: Target G/C by AID --- #
        spots_probs_dict = self.spots_probs
        spots = mark_spots(sequence)
        probs = np.array([spots_probs_dict[l] for l in spots])
        probs = probs / probs.sum()
        targets = np.random.choice(a=len(probs),
                                   size=n_mutations,
                                   replace=False,
                                   p=probs)
        sequence = np.array(list(sequence))
        mutated_sequence = sequence.copy()

        # --- Second phase: Replacement by BER/Replication of U-G mismatch --- #
        BER_targets = np.random.choice(a=targets,
                                       size=np.random.binomial(len(targets), self.BER_prob),
                                       replace=False)
        possible_bases = {'C': list(self.BER_transition_from_C.keys()),
                          'G': list(self.BER_transition_from_G.keys())}
        transition_probs = {'C': list(self.BER_transition_from_C.values()),
                            'G': list(self.BER_transition_from_G.values())}

        for t in BER_targets:
            original_base = sequence[t]
            mutated_sequence[t] = np.random.choice(a=possible_bases[original_base],
                                                   p=transition_probs[original_base])

        # --- Second phase: Target A/T and replacement by MMR --- #
        sigma = self.MMR_sigma
        MMR_centers = np.setdiff1d(targets, BER_targets)
        target_base_by_center = {'C': 'A', 'G': 'T'}
        possible_bases = {'A': list(self.MMR_transition_from_A.keys()),
                          'T': list(self.MMR_transition_from_T.keys())}
        transition_probs = {'A': list(self.MMR_transition_from_A.values()),
                            'T': list(self.MMR_transition_from_T.values())}

        for center in MMR_centers:
            target_base = target_base_by_center[sequence[center]]
            # Targeting
            candidate_targets = np.where(sequence == target_base)[0]
            candidate_targets_probs = norm.cdf(candidate_targets + 0.5, center, sigma) - \
                                      norm.cdf(candidate_targets - 0.5, center, sigma)
            candidate_targets_probs = candidate_targets_probs / candidate_targets_probs.sum()  # normalize
            target = np.random.choice(a=candidate_targets, size=1, p=candidate_targets_probs)
            # Substitution
            mutated_sequence[target] = np.random.choice(a=possible_bases[target_base],
                                                        p=transition_probs[target_base])
        return ''.join(mutated_sequence)

    def mutate_repertoire(self, data,
                          input_column='SEQUENCE_INPUT',
                          output_column='SEQUENCE_MUTATED',
                          mutations_per_sequence=10):
        data[output_column] = data[input_column].apply(lambda x: self.mutate_sequence(x, mutations_per_sequence))
        return data
