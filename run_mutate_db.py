import os
import platform
from simulation.simulation import Simulator
from utils.model_params import empty_mutability_table, empty_substitution_table


def main():

    # --- Platform dependant params --- #
    if platform.system() == 'Windows':
        input_file = r'c:\Users\דניאל\Dropbox\לימודים\תואר שני\מחקר\data\HCV\HCV_B\C10.tab'
        n_proc = 1
    elif platform.system() == 'Linux':
        input_file = r'~/data/HCV/HCV_B/C10.tab'
        n_proc = 47
    output_file = os.path.join(r'run_example', r'c10mut_2.tab')

    simulator = Simulator(n_proc=n_proc)

    # --- Set model parameters --- #
    simulator.n_mutations_per_sequence = 20

    mutability = empty_mutability_table()
    mutability.c_hot = 0.6
    mutability.c_neutral = 0.3
    mutability.c_cold = 0.1
    mutability.g_hot = 0.7
    mutability.g_neutral = 0.3
    simulator.mutability = mutability

    simulator.mmr_probabiliy = 0.5
    simulator.mmr_sigma = 10.3

    substitution = empty_substitution_table()
    substitution.c_hot = [0.0, 0.5, 0.3, 0.2]
    substitution.c_neutral = [0.0, 0.5, 0.3, 0.2]
    substitution.c_cold = [0.0, 0.5, 0.3, 0.2]
    substitution.g_hot = [0.4, 0.0, 0.3, 0.3]
    substitution.g_neutral = [0.5, 0.0, 0.2, 0.3]
    substitution.a = [0.5, 0.3, 0.0, 0.2]
    substitution.t = [0.5, 0.3, 0.2, 0.0]
    simulator.substitution = substitution

    # Run simulation
    mutated_repertoire = simulator.mutate_repertoire_io_wrapper(input_file=input_file,
                                                                original_sequence_column='SEQUENCE_IMGT',
                                                                output_file=output_file,
                                                                nrows=100)


if __name__ == '__main__':
    main()


