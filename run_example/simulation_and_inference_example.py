import platform
import pandas as pd
import numpy as np
from python.legacy.simulation import Model2P
from python.legacy.inference_2pm import infer_model_params
from python.legacy.utils import print_params

# --- Load data --- # 
# data_file = 'sample.csv'
# data = pd.read_csv(data_file, nrows=10)

if platform.system() == 'Windows':
    data_file = r'c:\Users\דניאל\Dropbox\לימודים\תואר שני\מחקר\data\HCV\HCV_B\C10.tab'
elif platform.system() == 'Linux':
    data_file = r'../../../data/HCV/HCV_B/C10.tab'

data = pd.read_csv(data_file, sep='\t', nrows=1000)

#  --- Set model parameters --- #
model_params = {'spots_probs': {'h': 0.5, 'n': 0.3, 'c': 0.2},
                'BER_prob': 1.0,
                'BER_transition_from_C': {'G': 0.4, 'A': 0.3, 'T': 0.3},
                'BER_transition_from_G': {'C': 0.6, 'A': 0.3, 'T': 0.1},
                'MMR_transition_from_A': {'C': 0.6, 'G': 0.3, 'T': 0.1},
                'MMR_transition_from_T': {'C': 0.5, 'G': 0.2, 'A': 0.3},
                'MMR_sigma': 7
                }

# --- Instantiate model object --- #
model = Model2P(model_params)

# --- Run simulation over data --- #
data = model.mutate_repertoire(data)

# --- Infer model params --- #
data_for_inference = pd.DataFrame({'original_sequence': data.SEQUENCE_INPUT,
                                   'mutated_sequence': data.SEQUENCE_MUTATED})
inferred_params = infer_model_params(data_for_inference,
                                     mmr_sigma_grid_coarse=np.arange(3, 17, 1),
                                     mmr_sigma_grid_fine=np.arange(-0.5, 0.5, 0.05))

# --- Prints --- #
print('------ True parameters of model ------')
print_params(model_params)
print('\n')

print('------ Inferred parameters of model ------')
print_params(inferred_params)
