import glob
import os
import pandas as pd
from python.legacy.simulation import Model2P

#  --- Set model parameters --- #
model_params = {'spots_probs': {'h': 0.5, 'n': 0.3, 'c': 0.2},
                'BER_prob': 1.0,
                'BER_transition_from_C': {'G': 0.4, 'A': 0.3, 'T': 0.3},
                'BER_transition_from_G': {'C': 0.6, 'A': 0.3, 'T': 0.1},
                'MMR_transition_from_A': {'C': 0.6, 'G': 0.3, 'T': 0.1},
                'MMR_transition_from_T': {'C': 0.5, 'G': 0.2, 'A': 0.3},
                'MMR_sigma': 7
                }

# --- Instantiate model object --- #
model = Model2P(model_params)

in_root = r'c:\Users\דניאל\Dropbox\לימודים\תואר שני\מחקר\data\HCV\HCV_B'
out_root = r'C:\Users\דניאל\Dropbox\לימודים\תואר שני\מחקר\data\synth_only_c'
for data_file in glob.glob(os.path.join(in_root, '*.tab')):
    data = pd.read_csv(data_file, sep='\t')

    # --- Run simulation over data --- #
    data = model.mutate_repertoire(data)

    # --- Save data --- #
    data = data[['SEQUENCE_INPUT', 'SEQUENCE_MUTATED']]
    out_file = os.path.join(out_root, data_file.split('\\')[-1])
    data.to_csv(out_file, index=False, sep='\t')
