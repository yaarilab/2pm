import os
import sys
import platform
from time import time
import pandas as pd
import numpy as np
from scipy.stats import pearsonr
from python.inference.mutability import Mutability
from python.inference.substitution import Substitution
from python.inference.stats import Stats



synonymous = bool(int(sys.argv[1]))
n_sequences = int(sys.argv[2])
n_smaples = 100

if platform.system() == 'Windows':
    data_root = r'c:\Users\דניאל\Dropbox\לימודים\תואר שני\מחקר\data\synth_only_c'
    nproc = 1
elif platform.system() == 'Linux':
    data_root = "~/data/synth_only_c"
    nproc = 1

data_file = os.path.join(data_root, 'all.tab')
data = pd.read_csv(data_file, sep='\t', nrows=n_sequences * n_smaples)

# TODO: generate random model params and mutate according to them
# TODO: Apply also to mutations in C and MMR
# TODO: Please complete the treatment of substitution


data = data.loc[:, ['SEQUENCE_INPUT', 'SEQUENCE_MUTATED']]
data.columns = ['original_sequence', 'mutated_sequence']  # Unify names


n_mutations = 0
substitution_estimation = []
substitution_true = []
mutability_estimation = []
mutability_true = []


for i in range(n_smaples):
    data_sample = data.iloc[(i * n_sequences):((i + 1) * n_sequences)]
    # --- Count mutations ---#
    count = Stats(only_synonymous=synonymous).profile_stats(data_sample, nproc)
    n_mutations += count.values.sum()

    # --- Infer substitution --- #
    t1 = time()
    substitution_table = Substitution(only_synonymous=synonymous).profile_substitution(data_sample, nproc)
    t2 = time()
    print('substitution inference took: ' + '{:.2f}'.format(t2 - t1) + 's')

    substitution_estimation.append(list((substitution_table[['c_hot', 'c_neutral', 'c_cold']] * [0.5, 0.3, 0.2]).sum(axis=1).values[1:]))
    substitution_true.append([0.4, 0.3, 0.3])

    # --- Infer substitution --- #
    t1 = time()
    mutability_table = Mutability(only_synonymous=synonymous).profile_mutability(data_sample, substitution_table, nproc)
    t2 = time()
    print('mutability inference took: ' + '{:.2f}'.format(t2 - t1) + 's')

    mutability_estimation.append(list(mutability_table[['c_hot', 'c_neutral', 'c_cold']].values))
    mutability_true.append([0.5, 0.3, 0.2])

n_mutations /= n_smaples
mutability_coef, _ = pearsonr(np.array(mutability_estimation).flatten(), np.array(mutability_true).flatten())
substitution_coef, _ = pearsonr(np.array(substitution_estimation).flatten(), np.array(substitution_true).flatten())

with open(os.path.join(data_root, 'new_convergence_test.txt'), 'a') as f:
    f.write('only synonymous: ' + str(synonymous) +
            ' | number of mutations: (avg.)' + str(n_mutations) +
            ' | Pearson correlation coefficient mutability: ' + str(mutability_coef) +
            ' | Pearson correlation coefficient substitution: ' + str(substitution_coef) +
            '\n')
