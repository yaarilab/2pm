import matplotlib.pyplot as plt
import numpy as np

# --- Convergence test: sigma --- #
order = np.array([1e2, 1e3, 1e4, 1e5, 1e6])
sigma_4_mse = np.array([2.03, 0.1, 0.01, 0.0, 0.01])
sigma_8_mse = np.array([7.56, 0.64, 0.08, 0.06, 0.04])
sigma_15_mse = np.array([4.1, 0.93, 0.32, 0.13, 0.11])

plt.plot(order, sigma_4_mse)
plt.plot(order, sigma_8_mse)
plt.plot(order, sigma_15_mse)
plt.xscale('log')
plt.title('Estimation convergence: $\sigma$')
plt.legend(['$\sigma = 4$', '$\sigma = 8$', '$\sigma = 15$'])
plt.xlabel('Number of mutations')
plt.ylabel('Mean Square error')
plt.show()

# --- Convergence test: categoricals --- #
order = np.array([1e2, 1e3, 1e4])
mean_kl_divergence_spots_probs = np.array([0.00897638981053053, 0.0011640616421334641, 0.0002542331036751929])
mean_kl_divergence_transition_from_C = np.array([0.005899235374265089, 0.0005753561964211267, 0.00013983965315770343])
mean_kl_divergence_transition_from_G = np.array([0.01349583438184877, 0.0015450009950178773, 0.0001262300103268774])
plt.plot(order, mean_kl_divergence_spots_probs)
plt.plot(order, mean_kl_divergence_transition_from_C)
plt.plot(order, mean_kl_divergence_transition_from_G)
plt.xscale('log')
plt.title('Estimation convergence: Categorical variables distribution')
plt.legend(['Spots', 'Transition from C', 'Transition from G'])
plt.xlabel('Number of mutations')
plt.ylabel('Mean KL divergence')
plt.show()
