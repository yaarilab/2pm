import pandas as pd
import numpy as np
from scipy.stats import entropy
from python.simulation.simulation_2pm import Model2P
from python.legacy.inference_2pm import infer_model_params



# # Version 1
# order = int(sys.argv[1])
# model_params = {'spots_probs': {'h': 0.5, 'n': 0.3, 'c': 0.2},
#                 'BER_prob': 0.5,
#                 'BER_transition_from_C': {'G': 0.4, 'A': 0.3, 'T': 0.3},
#                 'BER_transition_from_G': {'C': 0.6, 'A': 0.3, 'T': 0.1},
#                 'MMR_transition_from_A': {'C': 0.6, 'G': 0.3, 'T': 0.1},
#                 'MMR_transition_from_T': {'C': 0.5, 'G': 0.2, 'A': 0.3},
#                 'MMR_sigma': 7
#                 }
#
# N_iters = 10
# data_file = r'c:\Users\דניאל\Dropbox\לימודים\תואר שני\מחקר\data\CLIP_MS_3928_igblast_db-pass_clone-pass_germ-pass.csv'
# data = pd.read_csv(data_file, nrows=order/10)
# model = Model2P(model_params)
#
# params_names = ['h', 'n', 'c', 'C-G', 'C-A', 'C-T', 'G-C', 'G-A', 'G-T', 'A-C', 'A-G', 'A-T', 'T-C', 'T-G', 'T-A']
# estimated_params = pd.DataFrame(np.zeros((N_iters,len(params_names))), columns=params_names)
# for i in range(N_iters):
#     data_copy = data.copy()
#     data_copy = model.mutate_repertoire(data_copy)
#     data_copy = pd.DataFrame({'original_sequence': data_copy.SEQUENCE_INPUT,
#                               'mutated_sequence': data_copy.SEQUENCE_MUTATED})
#     inferred_params = infer_model_params(data_copy, mmr_sigma_grid=np.arange(3, 17, 1))
#     estimated_params['h'].iloc[i] = inferred_params['spots_probs']['h']
#     estimated_params['n'].iloc[i] = inferred_params['spots_probs']['n']
#     estimated_params['c'].iloc[i] = inferred_params['spots_probs']['c']
#     estimated_params['C-G'].iloc[i] = inferred_params['BER_transition_from_C']['G']
#     estimated_params['C-A'].iloc[i] = inferred_params['BER_transition_from_C']['A']
#     estimated_params['C-T'].iloc[i] = inferred_params['BER_transition_from_C']['T']
#     estimated_params['G-C'].iloc[i] = inferred_params['BER_transition_from_G']['C']
#     estimated_params['G-A'].iloc[i] = inferred_params['BER_transition_from_G']['A']
#     estimated_params['G-T'].iloc[i] = inferred_params['BER_transition_from_G']['T']
#     estimated_params['A-C'].iloc[i] = inferred_params['MMR_transition_from_A']['C']
#     estimated_params['A-G'].iloc[i] = inferred_params['MMR_transition_from_A']['G']
#     estimated_params['A-T'].iloc[i] = inferred_params['MMR_transition_from_A']['T']
#     estimated_params['T-C'].iloc[i] = inferred_params['MMR_transition_from_T']['C']
#     estimated_params['T-G'].iloc[i] = inferred_params['MMR_transition_from_T']['G']
#     estimated_params['T-A'].iloc[i] = inferred_params['MMR_transition_from_T']['A']
#
#
# summary = pd.DataFrame([estimated_params.mean(), estimated_params.std()])
# log_file = '..\..\..\data\convergance_test\convergence_test_results_others_' + 'order=' + str(order) + '.csv'
# summary.to_csv(log_file)


# Version 2 - KL divergence
orders = [1e2, 1e3, 1e4, 1e5]  # int(sys.argv[1])

model_params = {'spots_probs': {'h': 0.5, 'n': 0.3, 'c': 0.2},
                'BER_prob': 1.0,
                'BER_transition_from_C': {'G': 0.4, 'A': 0.3, 'T': 0.3},
                'BER_transition_from_G': {'C': 0.6, 'A': 0.3, 'T': 0.1},
                'MMR_transition_from_A': {'C': 0.6, 'G': 0.3, 'T': 0.1},
                'MMR_transition_from_T': {'C': 0.5, 'G': 0.2, 'A': 0.3},
                'MMR_sigma': 7
                }

N_iters = 10
data_file = r'c:\Users\דניאל\Dropbox\לימודים\תואר שני\מחקר\data\CLIP_MS_3928_igblast_db-pass_clone-pass_germ-pass.csv'
data = pd.read_csv(data_file, nrows=1e3)
model = Model2P(model_params)
for ord in orders:
    kl_divergence_spots = np.zeros(N_iters)
    kl_divergence_BER_transition_from_C = np.zeros(N_iters)
    kl_divergence_BER_transition_from_G = np.zeros(N_iters)
    for i in range(N_iters):
        data_copy = data.iloc[:int(ord/10)].copy()
        data_copy = model.mutate_repertoire(data_copy)
        data_copy = pd.DataFrame({'original_sequence': data_copy.SEQUENCE_INPUT,
                                  'mutated_sequence': data_copy.SEQUENCE_MUTATED})
        inferred_params = infer_model_params(data_copy,
                                             mmr_sigma_grid_coarse=np.arange(3, 17, 1),
                                             mmr_sigma_grid_fine=np.arange(-0.5, 0.5, 0.05),
                                             only_synonymous=True)
        pk = np.array([model_params['spots_probs']['h'],
                       model_params['spots_probs']['n'],
                       model_params['spots_probs']['c']])
        qk = np.array([inferred_params['spots_probs']['h'],
                       inferred_params['spots_probs']['n'],
                       inferred_params['spots_probs']['c']])
        kl_divergence_spots[i] = entropy(pk, qk)
        pk = np.array([model_params['BER_transition_from_C']['G'],
                       model_params['BER_transition_from_C']['A'],
                       model_params['BER_transition_from_C']['T']])
        qk = np.array([inferred_params['BER_transition_from_C']['G'],
                       inferred_params['BER_transition_from_C']['A'],
                       inferred_params['BER_transition_from_C']['T']])
        kl_divergence_BER_transition_from_C[i] = entropy(pk, qk)
        pk = np.array([model_params['BER_transition_from_G']['C'],
                       model_params['BER_transition_from_G']['A'],
                       model_params['BER_transition_from_G']['T']])
        qk = np.array([inferred_params['BER_transition_from_G']['C'],
                       inferred_params['BER_transition_from_G']['A'],
                       inferred_params['BER_transition_from_G']['T']])
        kl_divergence_BER_transition_from_G[i] = entropy(pk, qk)

    with open('convergence_test\kl_divergence_synonymous.txt', 'a') as f:
        f.write('order=' + str(ord) +
                ' mean_kl_divergence_spots=' + str(kl_divergence_spots.mean()) +
                ' mean_kl_divergence_transition_from_C=' + str(kl_divergence_BER_transition_from_C.mean()) +
                ' mean_kl_divergence_transition_from_G=' + str(kl_divergence_BER_transition_from_G.mean()) + '\n')


