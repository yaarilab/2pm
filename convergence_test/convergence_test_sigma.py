import sys
from time import ctime
import pandas as pd
import numpy as np
from python.simulation.simulation_2pm import Model2P
from python.legacy.inference_2pm import infer_model_params

# sigma = 7.2
# order = 1000
sigma = float(sys.argv[1])
order = int(sys.argv[2])

model_params = {'spots_probs': {'h': 0.5, 'n': 0.3, 'c': 0.2},
                'BER_prob': 0.5,
                'BER_transition_from_C': {'G': 0.4, 'A': 0.3, 'T': 0.3},
                'BER_transition_from_G': {'C': 0.6, 'A': 0.3, 'T': 0.1},
                'MMR_transition_from_A': {'C': 0.6, 'G': 0.3, 'T': 0.1},
                'MMR_transition_from_T': {'C': 0.5, 'G': 0.2, 'A': 0.3},
                'MMR_sigma': sigma
                }

N_iters = 10
data_file = r'c:\Users\דניאל\Dropbox\לימודים\תואר שני\מחקר\data\CLIP_MS_3928_igblast_db-pass_clone-pass_germ-pass.csv'
data = pd.read_csv(data_file, nrows=order/10)
model = Model2P(model_params)
estimated_sigma_vec = np.zeros(N_iters)
for i in range(N_iters):
    print('iter=' + str(i) + '   ' + ctime())
    data_copy = data.copy()
    data_copy = model.mutate_repertoire(data_copy)
    data_copy = pd.DataFrame({'original_sequence': data_copy.SEQUENCE_INPUT,
                              'mutated_sequence': data_copy.SEQUENCE_MUTATED})
    inferred_params = infer_model_params(data_copy,
                                         mmr_sigma_grid_coarse=np.arange(3, 17, 1),
                                         mmr_sigma_grid_fine=np.arange(-0.5, 0.5, 0.05),
                                         only_synonymous=True)
    estimated_sigma_vec[i] = inferred_params['MMR_sigma']
MSE = ((estimated_sigma_vec - sigma) ** 2).mean()

log_file = r'convergence_test\tmpDebug.txt'
with open(log_file, 'a') as f:
    f.write(''.join([ctime(), ' | '
                     ' sigma=', str(sigma),
                     ' order=', str(order),
                     ' MSE=', '{0:.2f}'.format(MSE),
                     '\n']))
