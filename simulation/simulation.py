import numpy as np
import pandas as pd
from scipy.stats import norm
from pathos.multiprocessing import ProcessPool
from utils.definitions import nucleotides
from utils.helper_functions import state_motif


class Simulator:
    def __init__(self, n_mutations_per_sequence=10, n_proc=1):
        # --- Model parameters --- #
        self.n_proc = n_proc
        self.n_mutations_per_sequence = n_mutations_per_sequence
        self.mutability = None
        self.substitution = None
        self.mmr_probabiliy = None
        self.mmr_sigma = None
        return

    def mutate_sequence(self, original_sequence):
        # TODO: Completely redesign! current approach is not very efficient
        original_sequence = np.array(list(original_sequence))

        if np.isin(original_sequence, nucleotides).sum() < 200:  # Case of bad sequence read, ignore
            return ''.join(original_sequence)

        pad = np.array(['N', 'N', 'N'])
        original_sequence = np.concatenate((pad, original_sequence, pad))
        mutated_sequence = original_sequence.copy()

        # Targeting - C nucleotides (BER)
        c_locs = np.where(original_sequence == 'C')[0]
        c_motifs = [state_motif(original_sequence[(loc - 2):(loc + 3)]) for loc in c_locs]
        c_probs = np.array([self.mutability[m] for m in c_motifs])
        c_probs = c_probs / c_probs.sum()
        c_targets = np.random.choice(a=c_locs,
                                     size=min(int(self.n_mutations_per_sequence / 2), len(c_locs)),  # Garbage data
                                     replace=False,
                                     p=c_probs)
        mmr_c_centers = np.random.choice(a=c_targets, size=int(len(c_targets) * self.mmr_probabiliy), replace=False)
        c_targets = np.setdiff1d(c_targets, mmr_c_centers)

        # Targeting - G nucleotides (BER)
        g_locs = np.where(original_sequence == 'G')[0]
        g_motifs = [state_motif(original_sequence[(loc - 2):(loc + 3)]) for loc in g_locs]
        g_probs = np.array([self.mutability[m] for m in g_motifs])
        g_probs = g_probs / g_probs.sum()
        g_targets = np.random.choice(a=g_locs,
                                     size=min(int(self.n_mutations_per_sequence / 2), len(g_locs)),  # Garbage data
                                     replace=False,
                                     p=g_probs)
        mmr_g_centers = np.random.choice(a=g_targets, size=int(len(g_targets) * self.mmr_probabiliy), replace=False)
        g_targets = np.setdiff1d(g_targets, mmr_g_centers)

        # Targeting - A nucleotides (MMR)
        a_targets = []
        candidate_targets = np.where(original_sequence == 'A')[0]
        for center in mmr_c_centers:
            candidate_targets = np.setdiff1d(candidate_targets, a_targets)  # Sampling without replacement

            # Filter far candidates
            inside_3_sigma = abs(candidate_targets - center) < (3 * self.mmr_sigma)
            relevant_candidates = candidate_targets[inside_3_sigma]
            if len(relevant_candidates) == 0:  # No relevant candidates
                continue

            # Calc probabilities and sample target
            relevant_candidates_probs = norm.cdf(relevant_candidates + 0.5, center, self.mmr_sigma) - \
                                      norm.cdf(relevant_candidates - 0.5, center, self.mmr_sigma)
            relevant_candidates_probs = relevant_candidates_probs / relevant_candidates_probs.sum()  # normalize
            target = int(np.random.choice(a=relevant_candidates, size=1, p=relevant_candidates_probs))
            a_targets.append(target)

        # Targeting - T nucleotides (MMR)
        t_targets = []
        candidate_targets = np.where(original_sequence == 'T')[0]
        for center in mmr_g_centers:
            candidate_targets = np.setdiff1d(candidate_targets, t_targets)  # Sampling without replacement

            # Filter far candidates
            inside_3_sigma = abs(candidate_targets - center) < (3 * self.mmr_sigma)
            relevant_candidates = candidate_targets[inside_3_sigma]
            if len(relevant_candidates) == 0:  # No relevant candidates
                continue

            # Calc probabilities and sample target
            relevant_candidates_probs = norm.cdf(relevant_candidates + 0.5, center, self.mmr_sigma) - \
                                      norm.cdf(relevant_candidates - 0.5, center, self.mmr_sigma)
            relevant_candidates_probs = relevant_candidates_probs / relevant_candidates_probs.sum()  # normalize
            target = int(np.random.choice(a=relevant_candidates, size=1, p=relevant_candidates_probs))
            t_targets.append(target)

        # Substitution - C nucleotides
        c_motifs_hot = c_locs[np.array(c_motifs) == 'c_hot']
        c_targets_hot = np.intersect1d(c_motifs_hot, c_targets)
        mutated_sequence[c_targets_hot] = np.random.choice(a=self.substitution.c_hot.keys(),
                                                           size=len(c_targets_hot),
                                                           p=self.substitution.c_hot.values)

        c_motifs_neutral = c_locs[np.array(c_motifs) == 'c_neutral']
        c_targets_neutral = np.intersect1d(c_motifs_neutral, c_targets)
        mutated_sequence[c_targets_neutral] = np.random.choice(a=self.substitution.c_neutral.keys(),
                                                               size=len(c_targets_neutral),
                                                               p=self.substitution.c_neutral.values)

        c_motifs_cold = c_locs[np.array(c_motifs) == 'c_cold']
        c_targets_cold = np.intersect1d(c_motifs_cold, c_targets)
        mutated_sequence[c_targets_cold] = np.random.choice(a=self.substitution.c_cold.keys(),
                                                            size=len(c_targets_cold),
                                                            p=self.substitution.c_cold.values)

        # Substitution - G nucleotides
        g_motifs_hot = g_locs[np.array(g_motifs) == 'g_hot']
        g_targets_hot = np.intersect1d(g_motifs_hot, g_targets)
        mutated_sequence[g_targets_hot] = np.random.choice(a=self.substitution.g_hot.keys(),
                                                           size=len(g_targets_hot),
                                                           p=self.substitution.g_hot.values)

        g_motifs_neutral = g_locs[np.array(g_motifs) == 'g_neutral']
        g_targets_neutral = np.intersect1d(g_motifs_neutral, g_targets)
        mutated_sequence[g_targets_neutral] = np.random.choice(a=self.substitution.g_neutral.keys(),
                                                               size=len(g_targets_neutral),
                                                               p=self.substitution.g_neutral.values)

        # Substitution - A nucleotides
        mutated_sequence[a_targets] = np.random.choice(a=self.substitution.a.keys(),
                                                       size=len(a_targets),
                                                       p=self.substitution.a.values)

        # Substitution - T nucleotides
        mutated_sequence[t_targets] = np.random.choice(a=self.substitution.t.keys(),
                                                       size=len(t_targets),
                                                       p=self.substitution.t.values)

        mutated_sequence = mutated_sequence[3:-3]
        mutated_sequence = ''.join(mutated_sequence)
        return mutated_sequence

    def apply_mutate_sequence(self, df):
        # TODO: Rename 'df'! here and in mutability/substitution code
        mutated_df = df.apply(lambda x: self.mutate_sequence(x))
        return mutated_df

    def mutate_repertoire(self, repertoire):
        """

        :param repertoire: pandas Sequence with original sequences
        :return: pandas Sequence with mutated sequences
        """

        if self.n_proc > 1:  # Multiprocess mode
            repertoire_split = np.array_split(repertoire, self.n_proc)
            pool = ProcessPool(self.n_proc)
            mutated_repertoire = pool.map(lambda x: self.apply_mutate_sequence(x), repertoire_split)
            mutated_repertoire = pd.concat(mutated_repertoire)  # Merge

        else:  # Single process - easier to debug
            mutated_repertoire = self.apply_mutate_sequence(repertoire)

        return mutated_repertoire

    def mutate_repertoire_io_wrapper(self, input_file, original_sequence_column, output_file=None, nrows=None):
        """

        :param input_file: Path to CSV or TAB file
        :param original_sequence_column: Upper case name, typical value: 'SEQUENCE_IMGT'
        :param output_file: File to save simulation products, if specified.
        :return:
        """

        ext = input_file.split('.')[-1]
        if ext == 'csv':
            sep = ','
        elif ext == 'tab':
            sep = "\t"

        data = pd.read_csv(input_file, sep=sep, nrows=nrows)
        repertoire = data[original_sequence_column]
        del data  # Free unusable RAM

        mutated_repertoire = self.mutate_repertoire(repertoire)

        if output_file:
            data = pd.read_csv(input_file, sep=sep, nrows=nrows)
            loc = int(np.where(data.columns == original_sequence_column)[0]) + 1
            mutated_column_name = original_sequence_column + '_MUTATED'
            data.insert(loc, mutated_column_name, mutated_repertoire)
            data.to_csv(output_file, sep=sep, index=False)

        return data

