from experiments.multiple_mutations_bias_effect.bootstrap import bootstrap


if __name__ == '__main__':
    bootstrap(n_mutations_per_strand=range(1, 13, 2),
              sigma=10)
