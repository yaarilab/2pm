import numpy as np


nucleotides = np.array(['C', 'G', 'A', 'T'])

# TODO: consider switching to numpy array instead of str, to reduce conversions load. do profiling!
codon_table = {'TTT': 'Phe', 'TTC': 'Phe', 'TTA': 'Leu', 'TTG': 'Leu',
               'TCT': 'Ser', 'TCC': 'Ser', 'TCA': 'Ser', 'TCG': 'Ser',
               'TAT': 'Tyr', 'TAC': 'Tyr', 'TAA': 'STOP', 'TAG': 'STOP',
               'TGT': 'Cyc', 'TGC': 'Cyc', 'TGA': 'STOP', 'TGG': 'Trp',
               'CTT': 'Leu', 'CTC': 'Leu', 'CTA': 'Leu', 'CTG': 'Leu',
               'CCT': 'Pro', 'CCC': 'Pro', 'CCA': 'Pro', 'CCG': 'Pro',
               'CAT': 'His', 'CAC': 'His', 'CAA': 'Gln', 'CAG': 'Gln',
               'CGT': 'Arg', 'CGC': 'Arg', 'CGA': 'Arg', 'CGG': 'Arg',
               'ATT': 'Ile', 'ATC': 'Ile', 'ATA': 'Ile', 'ATG': 'Met',
               'ACT': 'Thr', 'ACC': 'Thr', 'ACA': 'Thr', 'ACG': 'Thr',
               'AAT': 'Asn', 'AAC': 'Asn', 'AAA': 'Lis', 'AAG': 'Lis',
               'AGT': 'Ser', 'AGC': 'Ser', 'AGA': 'Arg', 'AGG': 'Arg',
               'GTT': 'Val', 'GTC': 'Val', 'GTA': 'Val', 'GTG': 'Val',
               'GCT': 'Ala', 'GCC': 'Ala', 'GCA': 'Ala', 'GCG': 'Ala',
               'GAT': 'Asp', 'GAC': 'Asp', 'GAA': 'Glu', 'GAG': 'Glu',
               'GGT': 'Gly', 'GGC': 'Gly', 'GGA': 'Gly', 'GGG': 'Gly'}

motifs = ['c_hot', 'g_hot', 'c_neutral', 'g_neutral', 'c_cold', 'a', 't']

DGYW = '([AGT])G([CT][AT])'  # G hotspot
DRCH = '([AGT][AG])C([ACT])'  # C hotspot
SYC = '([GC][CT])C'  # C coldspot

ignores = ['N', '.', '-']
