import re
import numpy as np
from utils.definitions import nucleotides, codon_table, SYC, DRCH, DGYW, ignores
from utils.model_params import *


def is_synonymous(orig_codon, mut_codon):
    # TODO: consider omitting the use of function for this functionality. do  profiling!
    if len(np.intersect1d(orig_codon, ignores)) > 0:
        return False
    orig_codon = ''.join(orig_codon)
    mut_codon = ''.join(mut_codon)
    return codon_table[orig_codon] == codon_table[mut_codon]


def is_only_synonymous(codon, mut_position):
    if len(np.intersect1d(codon, ignores)) > 0:
        return False
    all_possible = np.tile(codon, (4, 1))
    all_possible[:, mut_position] = np.array(list(nucleotides))
    codons = [''.join(c) for c in all_possible]
    aminoacid = [codon_table[c] for c in codons]
    if aminoacid.count(aminoacid[0]) == 4:  # All are same
        return True
    else:
        return False


def state_motif(target_neighborhood):
    target = target_neighborhood[2]
    if target == 'A':
        return 'a'
    elif target == 'T':
        return 't'
    elif target == 'C':
        if re.match(DRCH, ''.join(target_neighborhood[:4])):
            return 'c_hot'
        elif re.match(SYC, ''.join(target_neighborhood[:3])):
            return 'c_cold'
        else:
            return 'c_neutral'
    elif target == 'G':
        if re.match(DGYW, ''.join(target_neighborhood[1:])):
            return 'g_hot'
        else:
            return 'g_neutral'
    else:
        raise AssertionError('unrecognized base')


def calc_synonymousity_prob(sequence, locs, substitution_table):
    synonymousity_prob = np.zeros(locs.shape[0])

    for i, loc in enumerate(locs):
        context = sequence[(loc - 2):(loc + 3)]
        motif = state_motif(context)
        orig_codon = sequence[((loc // 3) * 3):((loc // 3 + 1) * 3)]
        mut_codon = orig_codon.copy()
        mut_pos = loc % 3
        for n in nucleotides:
            mut_codon[mut_pos] = n
            if is_synonymous(orig_codon, mut_codon):
                synonymousity_prob[i] += substitution_table[motif][n]

    return synonymousity_prob


def safe_divide(numerator, denominator):
    if ((denominator == 0) & (numerator != 0)).any():
        raise ZeroDivisionError
    denominator[denominator == 0] = 1.
    return numerator / denominator


def safe_normalization(vector):
    if np.all(vector == 0):
        return np.ones_like(vector)
    else:
        return vector / vector.sum()
