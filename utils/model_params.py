import numpy as np
import pandas as pd
from utils.definitions import nucleotides, motifs


def empty_substitution_table():
    return pd.DataFrame(data=0, index=nucleotides, columns=motifs)


def empty_mutability_table():
    return pd.Series(data=0.0, index=motifs[:-2])


def empty_stats_table():
    return pd.Series(data=0.0, index=motifs[:-2])


def empty_mmr_sigma_likelihoods(min, max, steps):
    return pd.Series(data=0.0, index=np.linspace(min, max, steps))
