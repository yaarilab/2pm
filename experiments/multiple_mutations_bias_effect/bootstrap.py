import sys
import os
import platform
from glob import glob
from random import sample
import numpy as np
import pandas as pd
from simulation.simulation import Simulator
from inference.mmr import MMR
from experiments.confidence_interval_mmr_estimation.crop_sequences import crop_sequences


def bootstrap(n_mutations_per_strand, sigma):
    # --- Estimations array --- #
    deltas = pd.Series(data=0.0, index=n_mutations_per_strand)

    # --- Platform dependant params --- #
    if platform.system() == 'Windows':
        experiment_dir = r'experiments\multiple_mutations_bias_effect'
        data_files_dir = r'c:\Users\דניאל\Dropbox\לימודים\תואר שני\מחקר\data\influenza'
        n_proc = 1
    elif platform.system() == 'Linux':
        experiment_dir = r'experiments/multiple_mutations_bias_effect'
        data_files_dir = r'/home/bcrlab/daniel/data/influenza'
        n_proc = 40

    print('Number of processes: ' + str(n_proc))

    # --- Load predefined fixed model params --- #
    substitution_table = pd.read_pickle(os.path.join(experiment_dir, 'substitution.pkl'))
    mutability_table = pd.read_pickle(os.path.join(experiment_dir, 'mutability.pkl'))

    # --- Instantiate estimator --- #
    mmr = MMR(mutability_table, substitution_table, only_synonymous=False, n_proc=n_proc)

    # --- Run experiment cycles --- #
    for n_mutations in n_mutations_per_strand:
        nrows = int(100000 / n_mutations)
        # Progress bar
        line = '\rExperiment running. ' + str(n_mutations) + ' Mutations per strand, Sigma = ' + str(sigma) + ', nrows = ' + str(nrows) + ' '
        print(line, end="")
        sys.stdout.flush()

        # Read data
        data_file = os.path.join(data_files_dir, 'FV_igblast_db-pass_functional_collapsed.tab')
        repertoire = pd.read_csv(data_file, sep='\t', nrows=nrows)
        repertoire = repertoire.loc[:, ['SEQUENCE_IMGT']]
        repertoire.columns = ['original_sequence']  # Unify names

        try:
            # Instantiate simulator
            simulator = Simulator(n_mutations_per_sequence=n_mutations * 2, n_proc=n_proc)
            simulator.mmr_probabiliy = 1.0
            simulator.mutability = mutability_table
            simulator.substitution = substitution_table

            # Simulate mutations
            simulator.mmr_sigma = sigma
            original_sequences = repertoire['original_sequence']
            mutated_sequences = simulator.mutate_repertoire(original_sequences)
            repertoire['mutated_sequence'] = mutated_sequences

            # Infer MMR sigma
            mmr_sigma_estimated = mmr.profile_mmr(repertoire,
                                                  lower_bound=1, upper_bound=20, num_steps=100, num_iterations=1)

            # Save results
            delta = mmr_sigma_estimated - sigma

            # --- Save to file --- #
            deltas[n_mutations] = delta
            deltas.to_pickle(os.path.join(experiment_dir, 'bias_sigma_' + str(sigma) + '.pkl'))

        except Exception:
            print('ERROR... skip sequence')

