import numpy as np


def crop(row, left_max_crop, right_max_crop):
    left_crop = np.random.randint(0, left_max_crop)
    right_crop = np.random.randint(0, right_max_crop)
    row.original_sequence = row.original_sequence[left_crop:-right_crop]
    row.mutated_sequence = row.mutated_sequence[left_crop:-right_crop]
    return row


def crop_sequences(repertoire, left_max_crop, right_max_crop):
    return repertoire.apply(lambda x: crop(x, left_max_crop, right_max_crop), axis=1)

