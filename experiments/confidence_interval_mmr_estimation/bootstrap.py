import sys
import os
import platform
from glob import glob
from random import sample
import numpy as np
import pandas as pd
from simulation.simulation import Simulator
from inference.mmr import MMR
from experiments.confidence_interval_mmr_estimation.crop_sequences import crop_sequences


def bootstrap(n_samples, n_sequences):
    # --- Estimations array --- #
    estimations = pd.DataFrame(data=0.0, index=range(n_samples), columns=['true', 'estimation'])

    # --- Platform dependant params --- #
    if platform.system() == 'Windows':
        experiment_dir = r'experiments\confidence_interval_mmr_estimation'
        data_files_dir = r'c:\Users\דניאל\Dropbox\לימודים\תואר שני\מחקר\data\HCV\HCV_B'
        n_proc = 3
    elif platform.system() == 'Linux':
        experiment_dir = r'experiments/confidence_interval_mmr_estimation'
        data_files_dir = r'/home/bcrlab/daniel/data/HCV/HCV_B'
        n_proc = 40

    print('Number of processes: ' + str(n_proc))
    all_sets = glob(os.path.join(data_files_dir, '*.tab'))

    # --- Load predefined fixed model params --- #
    substitution_table = pd.read_pickle(os.path.join(experiment_dir, 'substitution.pkl'))
    mutability_table = pd.read_pickle(os.path.join(experiment_dir, 'mutability.pkl'))

    # --- Instantiate simulator and estimator --- #
    simulator = Simulator(n_mutations_per_sequence=20, n_proc=n_proc)
    simulator.mmr_probabiliy = 1.0
    simulator.mutability = mutability_table
    simulator.substitution = substitution_table
    mmr = MMR(mutability_table, substitution_table, only_synonymous=False, n_proc=n_proc)

    # --- Run experiment cycles --- #
    for i in range(n_samples):
        # Progress bar
        line = '\rExperiment running. Sample ' + str(i + 1) + '/' + str(n_samples)
        print(line, end="")
        sys.stdout.flush()

        # Read data
        data_file = sample(all_sets, 1)[0]
        repertoire = pd.read_csv(data_file, sep='\t', nrows=n_sequences)
        repertoire = repertoire.loc[:, ['SEQUENCE_IMGT']]
        repertoire.columns = ['original_sequence']  # Unify names

        try:
            # Simulate mutations
            mmr_sigma = np.random.uniform(3, 12)
            simulator.mmr_sigma = mmr_sigma
            original_sequences = repertoire['original_sequence']
            mutated_sequences = simulator.mutate_repertoire(original_sequences)
            repertoire['mutated_sequence'] = mutated_sequences

            # Subsample each sequence
            repertoire = crop_sequences(repertoire, 20, 20)

            # Infer MMR sigma
            mmr_sigma_estimated = mmr.profile_mmr(repertoire,
                                                  lower_bound=2, upper_bound=15, num_steps=100, num_iterations=1)

            # Save results
            estimations.true[i] = mmr_sigma
            estimations.estimation[i] = mmr_sigma_estimated

            # --- Save to file --- #
            estimations.to_pickle(os.path.join(experiment_dir, 'mmr_sigma_estimation_' +
                                               str(n_samples) + '_samples_' +
                                               str(n_sequences) + '_sequences_each.pkl'))

        except Exception:
            print('ERROR... skip sequence')

