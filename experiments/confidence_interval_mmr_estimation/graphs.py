import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


results_dir = r'c:\Users\דניאל\Dropbox\לימודים\תואר שני\מחקר\code\python\2pm\experiments\confidence_interval_mmr_estimation'
n_mut = [1000, 10000, 100000]
fig, ax = plt.subplots()
ax.set_title('MMR sigma estimation: Confidence Interval')
ax.set_xlabel('Margin (symmetric)')
ax.set_ylabel('Confidence [%]')

for n in n_mut:
    res = pd.read_pickle(os.path.join(results_dir,
                                      'mmr_sigma_estimation_1000_samples_' + str(int(n / 2)) + '_sequences_each.pkl'))
    res = res[res.true != 0]
    N = res.shape[0]
    ci = np.arange(0.1, 2.4, .1)
    inside = np.abs((res.estimation.to_numpy() - res.true.to_numpy())) < ci[:, np.newaxis]
    percent = inside.sum(axis=1) / N * 100
    ax.plot(ci, percent)

ax.legend(['Using ' + str(n) + ' mutations' for n in n_mut])
plt.xticks(np.arange(0.2, 2.4, .2))
plt.yticks(range(10, 110, 10))
plt.axhline(y=95, color='r', linestyle='--')
plt.show()
