import os
import platform
import pandas as pd
from time import time
from inference.stats import Stats


def main():
    if platform.system() == 'Windows':
        data_root = 'run_example'
        nproc = 4
    elif platform.system() == 'Linux':
        data_root = "run_example"
        nproc = 47

    data_file = os.path.join(data_root, 'c10mut_2.tab')
    data = pd.read_csv(data_file, sep='\t', nrows=100000)
    data = data.loc[:, ['SEQUENCE_IMGT', 'SEQUENCE_IMGT_MUTATED']]
    # data = data.loc[:, ['SEQUENCE_INPUT', 'SEQUENCE_MUTATED']]
    data.columns = ['original_sequence', 'mutated_sequence']  # Unify names


    print('Number of processes: ' + str(nproc))

    t1 = time()
    stats = Stats(only_synonymous=False).profile_stats(data)
    t2 = time()
    print(stats)



if __name__ == '__main__':
    main()
